import dotenv from 'dotenv';
import express from 'express';
import { PORT, mongoDBURL } from './config.js';
import mongoose from 'mongoose';
import booksRoute from './routes/booksRoute.js';
import authRoute from './routes/authRoute.js';
import usersRoute from './routes/usersRoute.js';
import cors from 'cors';
import jwt from 'jsonwebtoken'
import cookieParser from 'cookie-parser'

dotenv.config();
const app = express();

app.use(express.json());
app.use(cors({
  origin: ["http://127.0.0.1", "http://localhost:5173"],
  methods: ['GET', 'POST', 'PUT', 'DELETE'],
  credentials: true
}));
app.use(cookieParser());

app.get('/', (request, response) => {
  console.log(request);
  return response.status(200).send('ok');
});

app.use('/auth', authRoute);
app.use('/books', booksRoute);
app.use('/users', usersRoute);

mongoose
  .connect(process.env.MONGO_URL)
  .then(() => {
    console.log('App connected to database');
    app.listen(PORT, () => {
      console.log(`App is listening to port: ${PORT}`);
    });
  })
  .catch((error) => {
    console.log(error);
  });
