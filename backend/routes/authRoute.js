import express from 'express';
import { User } from '../models/userModel.js';
import bcrypt from 'bcrypt'
import jwt from 'jsonwebtoken'

const router = express.Router();

router.post('/register', async (request, response) => {
    const {name, email, password} = request.body
    try {
        const hash = await bcrypt.hash(password, 10)
        await User.create({
            name,
            email,
            password: hash
        })
        response.json({status: 'Success'})
        
    } catch (error) {
        response.json({status: 'error', error: 'Duplicate email'})
    }
})

router.post('/login', async (request, response) => {
    const {name, email, password} = request.body
    try {
        const user = await User.findOne({
            email
        })
        if(user) {
            bcrypt.compare(password, user.password, (err, res) => {
                if(res) {
                    const token = jwt.sign({email: user.email, role: user.role}, "jwt-secret-key", {expiresIn: '1d'})
                    response.cookie('token', token)
                    return response.json({status: 'Success', user: {...user.toJSON(), token}})
                } else {
                    return response.json({status: 'error', error: 'The password is incorrect'})
                }
            })
        } else {
            return response.json({status: 'error', error: 'User does not exists'})
        }
    } catch (error) {
        return response.json({status: 'error', error: 'Unexpected error'})
    }
})

const varifyUser = (request, response, next) => {
    const token = request.cookies.token
    if(!token) {
        return response.json('Token is missing')
    } else {
        jwt.verify(token, "jwt-secret-key", (err, decoded) => {
            if(err) {
                return response.json('Error with token')
            } else {
                if(decoded.role === 'admin') {
                    next()
                } else {
                    return response.json('Not admin')
                }
            }
        })
    }
}

router.get('/dashboard', varifyUser, async (request, response) => {
    return response.json({status: 'Success'})
})

export default router
