import { useState } from 'react'
import { useAuthContext } from './useAuthContext'

export const useRegister = () => {
  const [error, setError] = useState(null)
  const [isLoading, setIsLoading] = useState(null)
  const { dispatch } = useAuthContext()

  const register = async (name, email, password) => {
    setIsLoading(true)
    setError(null)

    const response = await fetch('http://localhost:5555/auth/register', {
      method: 'POST',
      headers: {'Content-Type': 'application/json'},
      body: JSON.stringify({ name, email, password })
    })
    const json = await response.json()

    if (!response.ok) {
      setIsLoading(false)
      setError(json.error)
    }
    if (response.ok) {
      localStorage.setItem('user', JSON.stringify(json.user))
      dispatch({type: 'LOGIN', payload: json.user})
      setIsLoading(false)
    }
  }

  return { register, isLoading, error }
}