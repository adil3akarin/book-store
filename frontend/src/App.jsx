import React from 'react';
import { Routes, Route, Navigate } from 'react-router-dom';
import Home from './pages/Home';
import CreateBook from './pages/CreateBooks';
import ShowBook from './pages/ShowBook';
import EditBook from './pages/EditBook';
import DeleteBook from './pages/DeleteBook';
import Navbar from './components/Navbar';
import Registration from './pages/Registration';
import Login from './pages/Login';
import AdminDashboard from './pages/AdminDashboard';
import { useAuthContext } from './hooks/useAuthContext';
import DeleteUser from './pages/DeleteUser';
import EditUser from './pages/EditUser';

const App = () => {
  const { user } = useAuthContext()

  return (
    <>
    <Navbar/>
    <Routes>
      <Route path='/' element={<Home />} />
      <Route path='/registration' element={<Registration />} />
      <Route path='/login' element={<Login />} />
      <Route path='/books/create' element={user ? <CreateBook /> : <Navigate to={'/'}/>} />
      <Route path='/books/details/:id' element={<ShowBook />} />
      <Route path='/books/edit/:id' element={user ? <EditBook /> : <Navigate to={'/'}/>} />
      <Route path='/books/delete/:id' element={user ? <DeleteBook /> : <Navigate to={'/'}/>} />
      <Route path='/users/edit/:id' element={user ? <EditUser /> : <Navigate to={'/'}/>} />
      <Route path='/users/delete/:id' element={user ? <DeleteUser /> : <Navigate to={'/'}/>} />
      <Route path='/dashboard' element={user?.role === 'admin' ? <AdminDashboard /> : <Navigate to={'/'}/>} />
    </Routes>
    </>
  );
};

export default App;
