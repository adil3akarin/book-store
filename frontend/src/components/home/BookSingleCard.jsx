import { Link } from 'react-router-dom';
import { PiBookOpenTextLight } from 'react-icons/pi';
import { BiUserCircle, BiShow } from 'react-icons/bi';
import { AiOutlineEdit } from 'react-icons/ai';
import { BsInfoCircle } from 'react-icons/bs';
import { MdOutlineDelete } from 'react-icons/md';
import { useState } from 'react';
import BookModal from './BookModal';
import { useAuthContext } from '../../hooks/useAuthContext';

const BookSingleCard = ({ book }) => {
  const [showModal, setShowModal] = useState(false);
  const { user } = useAuthContext();

  return (
    <div className="card w-96 bg-base-100 shadow-xl card-normal gap-4">
      <figure><img src={book.image} alt={book.title} height="100" /></figure>
      <div className="card-body">
        <h2 className="card-title">
          {book.title}
          {
            book.publishYear && <div className="badge badge-secondary">NEW</div>
          }
        </h2>
        <div className="badge badge-outline">{book.publishYear}</div> 
        <p>If a dog chews shoes whose shoes does he choose?</p>
        <div className="card-actions justify-start">
          <span>{book.price}&#x20B8;</span>
        </div>
        {user?.role === 'admin' && (
          <div className='flex justify-around'>
            <BiShow
              className='text-3xl text-blue-800 hover:text-black cursor-pointer'
              onClick={() => setShowModal(true)}
            />
            <Link to={`/books/details/${book._id}`}>
              <BsInfoCircle className='text-2xl text-green-800 hover:text-black' />
            </Link>
            <Link to={`/books/edit/${book._id}`}>
              <AiOutlineEdit className='text-2xl text-yellow-600 hover:text-black' />
            </Link>
            <Link to={`/books/delete/${book._id}`}>
              <MdOutlineDelete className='text-2xl text-red-600 hover:text-black' />
            </Link>
          </div>
        )}
        <div className="card-actions justify-end">
          <button className="btn btn-primary">Купить</button>
        </div>
      </div>
      {showModal && (
        <BookModal book={book} onClose={() => setShowModal(false)} />
      )}
    </div>
  )
};

export default BookSingleCard;
