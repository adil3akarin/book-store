import React, { useState } from 'react'

const Loader = () => {
  const [elements, setEl] = useState(new Array(20).fill(''))
  
  return (
    // <span className="loading loading-dots loading-lg"></span>
    <div className='grid sm:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4'>
      {elements.map(() => (
        <div className="flex flex-col gap-4 m-8 p-8 w-96">
          <div className="skeleton bg-neutral h-32 w-full"></div>
          <div className="skeleton bg-neutral h-4 w-28"></div>
          <div className="skeleton bg-neutral h-4 w-full"></div>
          <div className="skeleton bg-neutral h-4 w-full"></div>
        </div>
      ))}
    </div>
  )
}

export default Loader