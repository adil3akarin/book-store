import React, { useEffect, useState } from 'react'
import axios from "axios";
import {Link, useNavigate} from "react-router-dom";
import { MdOutlineDelete } from 'react-icons/md';
import { AiOutlineEdit } from 'react-icons/ai';

const AdminDashboard = () => {
  const [success, setSuccess] = useState(false)
  const [users, setUsers] = useState([])
  const navigate = useNavigate()

  axios.defaults.withCredentials = true
  
  useEffect(() => {
    axios.get('http://localhost:5555/users')
      .then(res => {
        console.log(res.data);
        setUsers(res.data)
      })
  }, [])
  
  
  return (
    <div className="overflow-x-auto">
      <table className="table">
        <thead>
          <tr>
            <th></th>
            <th>Имя</th>
            <th>Email</th>
            <th>Роль</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          {users && users.map((user, index) => ( user &&
            <tr className="hover">
              <th>{index + 1}</th>
              <td>{user.name}</td>
              <td>{user.email}</td>
              <td>{user.role}</td>
              <td className='flex'>
                <Link to={`/users/edit/${user._id}`}>
                  <AiOutlineEdit className='text-2xl text-yellow-600 hover:text-black' />
                </Link>
                <Link to={`/users/delete/${user._id}`}>
                  <MdOutlineDelete className='text-2xl text-red-600 hover:text-black' />
                </Link>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  )
}

export default AdminDashboard