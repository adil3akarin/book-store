import React, { useState } from 'react';
import BackButton from '../components/BackButton';
import Loader from '../components/Loader';
import axios from 'axios';
import { useNavigate } from 'react-router-dom';
import { useSnackbar } from 'notistack';
import { useAuthContext } from '../hooks/useAuthContext';

const CreateBooks = () => {
  const [title, setTitle] = useState('');
  const [author, setAuthor] = useState('');
  const [publishYear, setPublishYear] = useState('');
  const [price, setPrice] = useState('');
  const [image, setImage] = useState('');
  const [loading, setLoading] = useState(false);
  const navigate = useNavigate();
  const { enqueueSnackbar } = useSnackbar();
  const { user } = useAuthContext()

  const handleSaveBook = () => {
    const data = {
      title,
      author,
      publishYear,
      price,
      image
    };
    setLoading(true);
    axios
      .post('http://localhost:5555/books', data, {
        headers: {
          'Authorization': `Bearer ${user.token}`
        }
      })
      .then(() => {
        setLoading(false);
        enqueueSnackbar('Book Created successfully', { variant: 'success' });
        navigate('/');
      })
      .catch((error) => {
        setLoading(false);
        enqueueSnackbar('Error', { variant: 'error' });
        console.log(error);
      });
  };

  return (
    <div className='p-4'>
      <BackButton />
      <h1 className='text-3xl my-4'>Добавить книгу</h1>
      {loading ? <Loader /> : ''}
      <div className='flex flex-col border-2 border-sky-400 rounded-xl w-[600px] p-4 mx-auto'>
        <div className='my-4'>
          <label className='label label-text'>Наименование</label>
          <input
            type='text'
            value={title}
            onChange={(e) => setTitle(e.target.value)}
            className='input input-bordered w-full'
          />
        </div>
        <div className='my-4'>
          <label className='label label-text'>Автор</label>
          <input
            type='text'
            value={author}
            onChange={(e) => setAuthor(e.target.value)}
            className='input input-bordered w-full '
          />
        </div>
        <div className='my-4'>
          <label className='label label-text'>Год издания</label>
          <input
            type='number'
            value={publishYear}
            onChange={(e) => setPublishYear(e.target.value)}
            className='input input-bordered w-full '
          />
        </div>
        <div className='my-4'>
          <label className='label label-text'>Цена</label>
          <input
            type='number'
            value={price}
            onChange={(e) => setPrice(e.target.value)}
            className='input input-bordered w-full '
          />
        </div>
        <div className='my-4'>
          <label className='label label-text'>Обложка</label>
          <input
            type='text'
            value={image}
            onChange={(e) => setImage(e.target.value)}
            className='input input-bordered w-full '
          />
        </div>
        <button className='btn btn-primary' onClick={handleSaveBook}>
          Добавить
        </button>
      </div>
    </div>
  );
}

export default CreateBooks