import React, { useState } from "react";
import axios from "axios";
import {Link, useNavigate} from 'react-router-dom'
import { useRegister } from "../hooks/useRegister";

const Registration = () => {
    const [name, setName] = useState()
    const [email, setEmail] = useState()
    const [password, setPassword] = useState()
    const navigate = useNavigate()

    const {register, error, isLoading} = useRegister()

    const handleSubmit = async (e) => {
      e.preventDefault()
  
      await register(name, email, password)
      navigate('/login')
    }
    
  return (
    <div className="hero h-100 mt-40 bg-base-200">
      <div className="hero-content flex-col lg:flex-row-reverse">
        <div className="card shrink-0 w-full max-w-sm shadow-2xl bg-base-100">
          <form className="card-body" onSubmit={handleSubmit}>
            <div className="form-control">
              <label className="label">
                <span className="label-text">Имя</span>
              </label>
              <input
                type="text"
                placeholder="Введите имя"
                className="input input-bordered"
                onChange={(e) => setName(e.target.value)}
                required
              />
            </div>
            <div className="form-control">
              <label className="label">
                <span className="label-text">Почта</span>
              </label>
              <input
                type="email"
                placeholder="Введите email"
                className="input input-bordered"
                onChange={(e) => setEmail(e.target.value)}
                required
              />
            </div>
            <div className="form-control">
              <label className="label">
                <span className="label-text">Пароль</span>
              </label>
              <input
                type="password"
                placeholder="Введите пароль"
                className="input input-bordered"
                onChange={(e) => setPassword(e.target.value)}
                required
              />
            </div>
            <div className="form-control mt-6">
                    <button className="btn btn-primary">Зарегистрироваться</button>
            </div>
          </form>
            <Link to="/login" className="btn">Войти</Link>
        </div>
        <div className="text-center lg:text-left">

    <img src="https://daisyui.com/images/stock/photo-1635805737707-575885ab0820.jpg" className="max-w-xl rounded-lg shadow-2xl" />
        </div>
      </div>
    </div>
  );
};

export default Registration;
