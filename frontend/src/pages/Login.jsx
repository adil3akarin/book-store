import React, { useState } from 'react'
import axios from 'axios'
import { Link, Navigate, useNavigate } from 'react-router-dom'
import { useLogin } from '../hooks/useLogin'

const Login = () => {
    const [email, setEmail] = useState()
    const [password, setPassword] = useState()
    const navigate = useNavigate()
    const {login, error, isLoading} = useLogin()
    axios.defaults.withCredentials = true

    const handleSubmit = async (e) => {
        e.preventDefault()

        login(email, password).then(() => {
            navigate('/')
        })
    }
    
  return (
    <div className="hero h-100 mt-40 bg-base-200">
        <div className="hero-content flex-col lg:flex-row-reverse">
            <div className="text-center lg:text-left">
                <h1 className="text-5xl font-bold">Bookify</h1>
                <p className="py-6">Bookify — не просто магазин книг, это место, где каждый читатель находит нечто особенное. Наш ассортимент насыщен лучшими произведениями литературы разных жанров: от захватывающих приключений и детективов до глубоких философских произведений и вдохновляющих бизнес-книг. У нас есть что-то для каждого, вне зависимости от ваших предпочтений.</p>
            </div>
            <div className="card shrink-0 w-full max-w-sm shadow-2xl bg-base-100">
                <form className="card-body" onSubmit={handleSubmit}>
                <div className="form-control">
                    <label className="label">
                        <span className="label-text">Почта</span>
                    </label>
                    <input
                        type="email"
                        placeholder="Введите email"
                        className="input input-bordered"
                        onChange={(e) => setEmail(e.target.value)}
                        required
                    />
                </div>
                <div className="form-control">
                <label className="label">
                    <span className="label-text">Пароль</span>
                </label>
                <input
                    type="password"
                    placeholder="Введите пароль"
                    className="input input-bordered"
                    onChange={(e) => setPassword(e.target.value)}
                    required
                />
                </div>
                <div className="form-control mt-6">
                    <button className="btn btn-primary">Войти</button>
                </div>
                </form>
                <Link to="/registration" className='btn'>Нет аккаунта</Link>
            </div>
        </div>
    </div>
  )
}

export default Login